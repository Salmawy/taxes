package com.bdc.from41.web.actions;

import com.haulmont.cuba.gui.components.ActionType;
import com.haulmont.cuba.gui.components.ListComponent;
import com.haulmont.cuba.gui.components.actions.ItemTrackingAction;

import javax.annotation.Nullable;

@ActionType("showReport")
public class ShowReportAction extends ItemTrackingAction {




    public ShowReportAction(String id) {

        super(id);
        setCaption("Show Selected");
    }

    public ShowReportAction(@Nullable ListComponent target, String id) {
        super(target, id);
        setCaption("Show Selected");
    }
}
