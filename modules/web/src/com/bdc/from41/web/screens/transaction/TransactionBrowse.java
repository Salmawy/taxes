package com.bdc.from41.web.screens.transaction;

import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.Transaction;
import org.slf4j.Logger;

import javax.inject.Inject;

@UiController("from41_Transaction.browse")
@UiDescriptor("transaction-browse.xml")
@LookupComponent("transactionsTable")
@LoadDataBeforeShow
public class TransactionBrowse extends StandardLookup<Transaction> {
    @Inject
    private Logger log;

    @Subscribe("transactionsTable.validate")
    public void onTransactionsTableValidate(Action.ActionPerformedEvent event) {
        log.info("button has been clicked");
    }
}