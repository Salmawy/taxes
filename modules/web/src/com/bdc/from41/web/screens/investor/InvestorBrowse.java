package com.bdc.from41.web.screens.investor;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.Investor;

@UiController("from41_Investor.browse")
@UiDescriptor("investor-browse.xml")
@LookupComponent("investorsTable")
@LoadDataBeforeShow
public class InvestorBrowse extends StandardLookup<Investor> {
}