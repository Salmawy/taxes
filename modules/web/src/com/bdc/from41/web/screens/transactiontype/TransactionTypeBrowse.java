package com.bdc.from41.web.screens.transactiontype;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.TransactionType;

@UiController("from41_TransactionType.browse")
@UiDescriptor("transaction-type-browse.xml")
@LookupComponent("transactionTypesTable")
@LoadDataBeforeShow
public class TransactionTypeBrowse extends StandardLookup<TransactionType> {
}