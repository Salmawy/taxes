package com.bdc.from41.web.screens.transactiontype;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.TransactionType;

@UiController("from41_TransactionType.edit")
@UiDescriptor("transaction-type-edit.xml")
@EditedEntityContainer("transactionTypeDc")
@LoadDataBeforeShow
public class TransactionTypeEdit extends StandardEditor<TransactionType> {
}