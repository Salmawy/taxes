package com.bdc.from41.web.screens.branch;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.Branch;

@UiController("Branch.browse")
@UiDescriptor("branch-browse.xml")
@LookupComponent("branchesTable")
@LoadDataBeforeShow
public class BranchBrowse extends StandardLookup<Branch> {
}