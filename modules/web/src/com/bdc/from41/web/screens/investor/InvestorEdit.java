package com.bdc.from41.web.screens.investor;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.Investor;

@UiController("from41_Investor.edit")
@UiDescriptor("investor-edit.xml")
@EditedEntityContainer("investorDc")
@LoadDataBeforeShow
public class InvestorEdit extends StandardEditor<Investor> {
}