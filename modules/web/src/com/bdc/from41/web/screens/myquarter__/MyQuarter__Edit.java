package com.bdc.from41.web.screens.myquarter__;

import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.MyQuarter__;

@UiController("from41_MyQuarter__.edit")
@UiDescriptor("my-quarter__-edit.xml")
@EditedEntityContainer("myQuarter__Dc")
@LoadDataBeforeShow
public class MyQuarter__Edit extends StandardEditor<MyQuarter__> {
}