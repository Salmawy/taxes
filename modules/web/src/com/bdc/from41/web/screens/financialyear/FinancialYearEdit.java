package com.bdc.from41.web.screens.financialyear;

import com.bdc.from41.service.ITransactionService;
import com.haulmont.cuba.core.app.DataService;
import com.haulmont.cuba.core.app.PersistenceManagerService;
import com.haulmont.cuba.gui.screen.*;
import com.bdc.from41.entity.FinancialYear;

import javax.inject.Inject;
import java.text.SimpleDateFormat;
import java.util.Date;

@UiController("FinancialYear.edit")
@UiDescriptor("financial-year-edit.xml")
@EditedEntityContainer("financialYearDc")
@LoadDataBeforeShow
public class FinancialYearEdit extends StandardEditor<FinancialYear> {

    @Inject
    private PersistenceManagerService persistenceManagerService;
    @Inject
    private ITransactionService transactionService;

    @Subscribe
    public void onBeforeCommitChanges(BeforeCommitChangesEvent event) {
       transactionService.restFinancialYears(this.getEditedEntity().getId());
        String yearCode=getFinancialYearCode(   this.getEditedEntity().getFromDate());
        this.getEditedEntity().setFinYearCode(yearCode);
        event.resume();


       }

    @Subscribe
    public void onAfterClose(AfterCloseEvent event) {



    }

private String getFinancialYearCode(Date year){
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy");
    return "FY"+sdf.format(year);
}


}