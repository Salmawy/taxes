package com.bdc.from41.service;

import com.bdc.from41.core.enums.QuarterMonthesEnum;
import com.bdc.from41.core.enums.TransactionStatusEnum;
import com.bdc.from41.entity.FinancialYear;
import com.bdc.from41.entity.MyQuarter__;
import com.bdc.from41.entity.Transaction;
import com.bdc.from41.entity.TransactionStatus;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.TransactionalDataManager;
import com.haulmont.cuba.core.global.DataManager;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.*;

@Service(ITransactionService.NAME)
public class TransactionServiceBean implements ITransactionService {
    @Inject
    private DataManager dataManager;
    @Inject
    private Persistence persistence;
    @Inject
    private TransactionalDataManager txDataManager;
    @Inject
    private Logger log;

    @Override
    public List<MyQuarter__> getSuggestedQuarters() {



        Date currentDate=new Date();
        
        int currentQuarter=this.getCurrentQuarter(currentDate);
        int pastQuarter=this.getPastQuarter(currentDate);

        MyQuarter__ q= dataManager.load(MyQuarter__.class).id(currentQuarter).one();

        List quarters=new ArrayList(Arrays.asList(q));
        if (pastQuarter!=0)quarters.add( dataManager.load(MyQuarter__.class).id(pastQuarter).one());
        return quarters;
    }


    private int getCurrentQuarter( Date currentDate){

        int[] quarters ={1,4,7,10,13};
        SimpleDateFormat sdf=new SimpleDateFormat("MM");
        int currentMonth=Integer.parseInt(sdf.format(currentDate));

        int i;
        for ( i=0;i<quarters.length;i++){
            if (currentMonth >= quarters[i]&&currentMonth<quarters[i+1]) {
                break;
            }
        }
return i+1;

    }


    private int getPastQuarter( Date currentDate){

         SimpleDateFormat sdf=new SimpleDateFormat("MM");
        int currentMonth=Integer.parseInt(sdf.format(currentDate));

       if(currentMonth==1)return 4;
        if(currentMonth==4)return 1;
        if(currentMonth==7)return 2;
        if(currentMonth==10)return 3;
        return 0;
    }
    @Override
    public FinancialYear getActiveFinancialYears(){

        FinancialYear  activeFinancialYear= this.dataManager.load(FinancialYear.class).query("e.active = '1'").one();
        return activeFinancialYear;

    }
@Override
    public void restFinancialYears(int id){

        List <FinancialYear> fns= this.dataManager.load(FinancialYear.class).query("e.active = '1' and e.id <> ?1",id).list();
        for (FinancialYear fn :fns) {
           fn.setActive(false);
            txDataManager.save(fn);
        }

    }
@Override
public List getTransaction(int finYearId, int branchCodeId, int quarterId, int statusId){



        StringBuilder query=new StringBuilder("select e from Transaction e where 1=1 ");
        if(finYearId!=0){
            query.append(" and e.financialYear.id="+finYearId);
        }

    if(branchCodeId!=0){
        query.append("  and e.branchCode.id="+branchCodeId);
    }

    if(quarterId!=0){
        query.append("  and e.quarter.id="+quarterId);
    }

    if(statusId!=0){
        query.append("  and e.status.id="+statusId);
    }

    List transactions = dataManager.load(Transaction.class).query(query.toString()).view("transactionBrowseView").list();
            return transactions;
    }


    @Override
    public int validateQuartertransactions(int yearId, String branchCode, int quarterId){
      Optional<BigDecimal>GLBalance=Optional.of(getQuarterGLBlance(yearId, branchCode, quarterId));
        Optional<BigDecimal>TransactionBalance=Optional.of(getQuarterTransactionsBlance(yearId, branchCode, quarterId));
        TransactionBalance.orElse(new BigDecimal(0.0));
        GLBalance.orElse(new BigDecimal(0.0));



            log.info("GL Balance =>"+(GLBalance.get().abs().doubleValue()) );




            log.info("Transaction Balance =>"+TransactionBalance.get().toString());
        int value=  Double.compare(TransactionBalance.get().abs().doubleValue(),GLBalance.get().abs().doubleValue());
if (value==0){


    changeTransactionTovalidatedStatus(yearId,branchCode,quarterId);

}
        return value;




    }
    @Override
    public  BigDecimal getQuarterTransactionsBlance(int yearId, String branchCode, int quarterId) {

        com.haulmont.cuba.core.Transaction tx= persistence.getTransaction();


        String query="  select sum(e.grossAmount)from Transaction e " +
                "where  e.financialYear.id like :year" +
                " and e.branch.code like :branchCode " +
                "and e.quarter.id = :quarterId";



        Query queryRunner=persistence.getEntityManager().createQuery(query);
        queryRunner.setParameter("year",yearId);
        queryRunner.setParameter("branchCode",branchCode);
        queryRunner.setParameter("quarterId",quarterId);
        Double balance=(Double)queryRunner.getSingleResult();
        tx.commit();
        tx.end();
        return new BigDecimal(balance);
    }


    @Override
    @Transactional
    public  void changeTransactionTovalidatedStatus(int yearId, String branchCode, int quarterId) {




        TransactionStatus validatedStatus=   dataManager.load(TransactionStatus.class).
                query("  e.id like ?1",TransactionStatusEnum.VALIDATED.getId()).one();


//        ==========================================================================================================================
        Map<String,Object> params=new HashMap<String,Object> ();
        params.put("year",yearId);
        params.put("branchCode",branchCode);
        params.put("quarterId",quarterId);
        params.put("status", TransactionStatusEnum.SAVED.getId());
        String query=" e.financialYear.id like :year" +
                " and e.branch.code like :branchCode " +
                " and e.status.id like :status " +
                "and e.quarter.id = :quarterId";

     List<Transaction> savedTransactions=   dataManager.load(Transaction.class).query(query).setParameters(params).view("transactionBrowseView").list();


     for (Transaction t:savedTransactions){
         t.setStatus(validatedStatus);
         dataManager.commit(t);

     }



     }

    @Override


    //@Transactional(propagation = Propagation.REQUIRES_NEW)
    public BigDecimal getQuarterGLBlance(int yearId, String branchCode, int quarterId) {
        com.haulmont.cuba.core.Transaction tx= persistence.getTransaction();
         FinancialYear year=dataManager.load(FinancialYear.class).id(yearId).one();
//      persistence.createTransaction();
        String []monthes=null;
        switch (quarterId){
            case 1:monthes= QuarterMonthesEnum.fristQuarter[1].split(",") ;break;
            case 2:monthes= QuarterMonthesEnum.secondQuarter[1].split(",")  ;break;
            case 3:monthes= QuarterMonthesEnum.thirdQuarter[1].split(",")  ;break;
            case 4:monthes= QuarterMonthesEnum.fourthQuarter[1].split(",")  ;break;

        }

        String query="  select sum(e.balance) from GLBlance e where  e.finYear like :year and e.branchCode like :branchCode and e.periodCode in :periodCode";


          Query queryRunner=persistence.getEntityManager().createQuery(query);
        queryRunner.setParameter("year",year.getFinYearCode());
        queryRunner.setParameter("branchCode",branchCode);
        queryRunner.setParameter("periodCode",new ArrayList<>(Arrays.asList(monthes)));
        BigDecimal balance=(BigDecimal)queryRunner.getSingleResult();
        tx.commit();
        tx.end();
    return balance;
//        return BigDecimal.ZERO;
    }


}