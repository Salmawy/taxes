DROP VIEW TRANSACTION_REPORT;

/* Formatted on 11/22/2020 10:07:44 PM (QP5 v5.185.11230.41888) */
CREATE OR REPLACE FORCE VIEW TRANSACTION_REPORT
AS
   SELECT
   TRX.ID AS ID ,
   q.ID AS QUARTER,
          FN.NAME AS FIN_YEAR,
          TRX.REGISTERATION_NUMBER,
          '' AS FILE_NO,
          '' AS MASSION_CODE,
          TRX.TRANSACTION_DATE,
          tr_Type.code,
          TRX.GROSS_AMOUNT,
          TRX.DISCOUNT_TYPE,
          TRX.NET_AMOUNT,
          tr_Type.COMMISSION,
          TRX.GROSS_AMOUNT * tr_Type.COMMISSION AS TOTAL_TAX,
          INVESTOR.NAME AS INVESTOR_NAME,
          INVESTOR.ADDRESS AS INVESTOR_ADDRESS,
          INVESTOR.NATIONAL_ID,
          TRX.CURENCY
     FROM transactions trx,
          financial_years fn,
          transaction_type tr_Type,
          Quarters q,
          INVESTORS INVESTOR
    WHERE     1 = 1
          AND TRX.FINANCIAL_YEAR_ID = fn.id
          AND TRX.QUARTER_ID = q.id
          AND TRX.TRANSACTION_TYPE_ID = tr_Type.id
          AND TRX.INVESTOR_ID =INVESTOR.ID(+)
