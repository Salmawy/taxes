package com.bdc.from41.entity;

import com.bdc.from41.core.enums.TransactionStatusEnum;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import com.haulmont.cuba.core.entity.HasUuid;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Table(name = "TRANSACTIONS")
@Entity(name = "Transaction")
public class Transaction extends BaseIntegerIdEntity implements HasUuid {
    private static final long serialVersionUID = 5283695278870978490L;

    @Column(name = "UUID")
    private UUID uuid;


    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRANCH_CODE_ID")
    @NotNull
    private Branch branch;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "FINANCIAL_YEAR_ID")
    private FinancialYear financialYear;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "QUARTER_ID")
    private MyQuarter__ quarter;

    @Column(name = "DISCOUNT_TYPE")
    private Double discountType = 1.0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INVESTOR_ID")
    private Investor investor;

    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @ManyToOne(optional = false)
    @JoinColumn(name = "TRANSACTION_TYPE_ID")
    @NotNull
    private TransactionType transactionType;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Column(name = "TRANSACTION_DATE", nullable = false)
    private Date transactionDate;

    @Column(name = "CURENCY")
    private Integer curency = 1;

    @Column(name = "GROSS_AMOUNT", nullable = false)
    @DecimalMin(message = "{msg://err.grossAmount.valueMustbe300orGreater}", value = "300")
    @NotNull
    private Double grossAmount;

    @Column(name = "NET_AMOUNT")
    private Double netAmount;

    @Column(name = "REGISTERATION_NUMBER", unique = true, columnDefinition = "varchar(255)")
    @Min(message = "min length must is 9 digits", value = 9)
    private Integer registerationNumber;


    @Lookup(type = LookupType.DROPDOWN, actions = "lookup")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS_ID")
    private TransactionStatus status = new TransactionStatus(TransactionStatusEnum.VALIDATED.name());

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Branch getBranch() {
        return branch;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public Double getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(Double grossAmount) {
        this.grossAmount = grossAmount;
    }

    public Integer getCurency() {
        return curency;
    }

    public void setCurency(Integer curency) {
        this.curency = curency;
    }

    public Double getDiscountType() {
        return discountType;
    }

    public void setDiscountType(Double discountType) {
        this.discountType = discountType;
    }

    public Integer getRegisterationNumber() {
        return registerationNumber;
    }

    public void setRegisterationNumber(Integer registerationNumber) {
        this.registerationNumber = registerationNumber;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public MyQuarter__ getQuarter() {
        return quarter;
    }

    public void setQuarter(MyQuarter__ quarter) {
        this.quarter = quarter;
    }

    public FinancialYear getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(FinancialYear financialYear) {
        this.financialYear = financialYear;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

}