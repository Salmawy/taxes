package com.bdc.from41.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;
import lombok.AllArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "TRANSACTION_STATUS")
@Entity(name = "from41_TransactionStatus")
@NamePattern("%s %s|name,nameAr")
@AllArgsConstructor
public class TransactionStatus extends BaseIntegerIdEntity {
    private static final long serialVersionUID = 5440782452545055678L;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NAME_AR")
    private String nameAr;

    public TransactionStatus() {

    }

    public TransactionStatus(String name) {
        this.name = name;
        this.setId(1);
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}