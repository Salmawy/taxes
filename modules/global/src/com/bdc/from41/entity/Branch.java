package com.bdc.from41.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseIntegerIdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "BRANCHES")
@Entity(name = "Branch")
@NamePattern("%s %s|code,branchName")
public class Branch extends BaseIntegerIdEntity {
    private static final long serialVersionUID = -4591801954925520119L;

    @Column(name = "BRANCH_CODE", nullable = false, unique = true, length = 15)
    @NotNull
    private String code;

    @Column(name = "BRANCH_LOCATION", length = 1024)
    private String branchLocation;

    @Column(name = "BRANCH_TYPE")
    private String branchType;

    @Column(name = "BRANCH_ADDRESS", length = 1024)
    private String branchAddress;

    @Column(name = "BRANCH_ADDRESS2", length = 1024)
    private String branchAddress2;

    @Column(name = "BRANCH_ADDRESS3", length = 1024)
    private String branchAddress3;

    @Column(name = "BRANCH_STATUS")
    private String branchStatus;

    @Column(name = "BRANCH_NAME", length = 1024)
    private String branchName;

    @Column(name = "BRANCH_COUNTRY")
    private String branchCountry;

    public String getBranchCountry() {
        return branchCountry;
    }

    public void setBranchCountry(String branchCountry) {
        this.branchCountry = branchCountry;
    }


    public String getBranchName() {
        return branchName;
    }

    public String getBranchStatus() {
        return branchStatus;
    }

    public String getBranchAddress3() {
        return branchAddress3;
    }

    public String getBranchAddress2() {
        return branchAddress2;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public String getBranchType() {
        return branchType;
    }

    public String getBranchLocation() {
        return branchLocation;
    }

    public String getCode() {
        return code;
    }

}